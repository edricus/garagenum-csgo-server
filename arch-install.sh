#!/bin/bash
function ask-steampath () {
	while true; do
	echo "Emplacement du dossier steam ? "
	echo "L'emplacement doit se terminer par un '/'"
	read -e -p "laisser vide par defaut : " steampath
	if [[ -z $steampath ]]; then
		steampath = /home/$USER/.steam
		break
	elif [[ ! -d $steampath ]]; then
		echo "$steampath n'existe pas, réessayez"
	else break
	fi
	done
}

read -p "[I]nstall, [S]tart, [U]pdate, [R]attach " response
if [[ $response == 'I' || $response == 'i' ]]; then
	ask-steampath
	## DEPS ##
	sudo pacman -Sy lib32-gcc-libs tmux
	curl -O https://aur.archlinux.org/cgit/aur.git/snapshot/steamcmd.tar.gz
	tar -xvzf steamcmd.tar.gz
	cd steamcmd
	makepkg -si
	sudo pacman -U *.pkg.tar.xz
	## USER ##
	sudo groupadd csgo
	sudo mkdir /var/lib/csgo
	sudo useradd -d /var/lib/csgo -g csgo -s /bin/bash csgo
	sudo chown csgo.csgo -R /var/lib/csgo
	## INSTALL ##
	sudo -u csgo /usr/games/steamcmd +login anonymous +force_install_dir $steampath +app_update 740 validate +quit
	## CONF ##
	if [[ $steampath -ne "/home/$USER/.steam" ]]; then
		echo 'remplacer /usr/lib/ par le dossier steam'
		sleep 3
		vim csgo.sh
	fi
	sudo install -D -o csgo -m 775 csgo.sh /home/csgo/server/csgo.sh
	sudo install -D -o csgo server.cfg /home/csgo/server/cfg/server.cfg
	sudo install -D csgo.service /usr/lib/systemd/system/csgo.service
	read -p "Démarrer ? (Y/n) " response2
	if [[ $response2 == 'y' || $response2 == 'Y' || -z $response2 ]]; then
		sudo systemctl start csgo.service
		echo 'Server started successfully'
	fi
fi

if [[ $response == 'S' || $response == 's' ]]; then
	sudo systemctl start csgo.service
	echo 'Server started successfully'
fi

if [[ $response == 'U' || $response == 'u' ]]; then
	ask-steampath
	steamcmd +quit
	sudo systemctl stop csgo.service
	sudo -u csgo /usr/games/steamcmd +login anonymous +force_install_dir $steampath +app_update 740 validate +quit
	sudo systemctl start csgo.service
	echo 'Server started successfully'
fi

if [[ $response == 'R' || $response == 'r' ]]; then
	echo 'Rattaching in 5s...'
	echo "To quit without killing the server press 'CTRL+b' and then 'd'"
	sleep 5
	sudo -u csgo tmux attach -t csgo-console
fi
exit 0
