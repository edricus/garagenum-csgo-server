#!/bin/sh

USER=$2

if [ -z $2 ]; then
  USER="csgo"
fi

case "$1" in
  start)
    sudo -u csgo tmux new-session -d -s csgo-console -d 'cd /var/lib/csgo/server/; /var/lib/csgo/server/srcds_run -console -game csgo -usercon +game_type 0 +game_mode 0 +mapgroup mg_active +map de_dust2'
    ;;

  stop)
    sudo -u csgo tmux send-keys -t csgo-console 'say Server shutting down in 10 seconds!' C-m
    sleep 10
    sudo -u csgo tmux send-keys -t csgo-console 'quit' C-m
    sleep 5
    ;;

  *)
    echo "Usage: $0  user"
esac

exit 0
